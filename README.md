# Taller de "Recuperación": Implementación de Estructuras de Datos en Java/Python

**Objetivo del Taller:**
Desarrollar un programa en Java o Python que permita a los estudiantes interactuar con diversas estructuras de datos mediante un menú. Se espera que los estudiantes implementen operaciones de inserción, búsqueda y actualización para cada estructura de datos y proporcionen una visualización gráfica de los grafos y árboles.

**Estructuras de Datos Cubiertas:**

- Búsqueda en anchura (BFS)
- Búsqueda en profundidad (DFS)
- Árboles binarios
- Algoritmo de Dijkstra
- Pilas (usando listas)
- Colas (usando listas)
- Árboles B+ (inserción)

**Requisitos del Taller:**

### Desarrollo del Programa:

1. Implementar un menú principal para acceder a cada estructura de datos.
2. Implementar submenús para cada estructura, permitiendo operaciones de inserción, búsqueda y actualización.
3. Visualización gráfica de grafos y árboles:
   - En Python, se puede utilizar bibliotecas como `matplotlib` y `networkx` para la visualización.
   - En Java, se puede utilizar `Swing` para la visualización.

### Documentación del Programa:

1. Incluir comentarios detallados en el código explicando cada función y su propósito.
2. Proporcionar un reporte en LaTeX de no menos de 3 hojas que explique cada parte del programa y la complejidad computacional de cada algoritmo implementado.

**Instrucciones para los Estudiantes:**

1. Implementar el programa en Java o Python según las especificaciones.
2. Asegurarse de que cada estructura de datos tenga su propio submenú para operaciones de inserción, búsqueda y actualización.
3. Proporcionar visualización gráfica para grafos y árboles.
4. Documentar el código y preparar un reporte en LaTeX explicando la implementación y complejidad computacional de cada algoritmo.
5. Subir el código y el reporte completado en el formato especificado a GIT.
6. ENTREGA INDIVIDUAL
7. SI SE DETECTA COPIA SE ANULARA Y REPORTARA al estudiante.
8. La sustentacion sera INDIVIDUAL - MAX 5 MIN
9. Plazo maximo 8 de Junio
8. No se recibe si no esta en el Repositorio

